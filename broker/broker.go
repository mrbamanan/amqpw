// Package broker implements a broker to abstract amqp interaction. Exposes simple consumer & producer
package broker

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/rabbitmq/amqp091-go"

	"gitlab.com/qosenergy/amqpw/internal/common"
	"gitlab.com/qosenergy/amqpw/internal/connection"
	"gitlab.com/qosenergy/amqpw/internal/consumer"
	"gitlab.com/qosenergy/amqpw/internal/producer"
	"gitlab.com/qosenergy/amqpw/internal/tools"
)

// Env is the preferred amqpw specific env vars
type Env struct {
	URI string `env:"RMQ_SERVER,required=true"`
}

// Conf regroups all conf required for a broker
type Conf struct {
	// URI format expected:
	//   "amqp://$(AMQP_LOGIN):$(AMQP_PASSWORD)@$(AMQP_ADDRESS)/$(RMQ_VHOST)"
	URI string
	// ConnName will label the connection in rabbitMQ. Use it for monitoring & tracing purposes
	ConnName string
	// Consumer defines the conf for the consumer. No consumer will be available if nil
	Consumer *ConsumerConf
	// Producer defines the conf for the producer. No producer will be available if nil
	Producer *ProducerConf
}

// ConsumerConf is the required conf for a consumer.
type ConsumerConf struct {
	// ChannelPoolSize should not be bigger than the number of queue we expect to consume.
	// It should also not be too big (rabbitMQ has it's limitations).
	// Value will be 1 if the given value is smaller than 1.
	ChannelPoolSize int
	// QOS defines the AMQP prefetch policy.
	// Use [NewQOS] to create it.
	// No QOS will be used if QOS is nil (no limit to number of locked message, ill advised).
	QOS *common.QOS
}

// ProducerConf is the required conf for a producer.
type ProducerConf struct {
	// ChannelPoolSize will be considered 1 if the given value is smaller than 1.
	// ChannelPoolSize should also not be too big (rabbitMQ has it's limitations)
	ChannelPoolSize int
	// ConfirmationTimeout (default 10s) defines the max time to wait for amqp publish confirm.
	ConfirmationTimeout time.Duration
	// ForcePersistence true forces persistent parameter for every message, to force writing on disk & prevent loss.
	ForcePersistence bool
	// ForceMandatory true forces mandatory parameter for every message:
	// publish will be rejected unless the message is written in at least 1 queue.
	ForceMandatory bool
	/*
	 ForceInfiniteRejectPublish forces all messages to infinitely retry on reject-publish.
	 Useful to avoid error if the output queue is full and in reject-publish mode.
	 Use [amqpwerror.IsErrPublishRejected]() on StatusChan output if you need to detect them.
	 For safety reasons, this option had no effect when statusChan is nil
	 (including [Broker.Publish] & [Broker.PublishWithRetry] func).
	 Retry timer on reject publish is currently fixed at 10 seconds.

	 WARNING: this can lead to endless block with mandatory and bad RabbitMQ topology.
	*/
	ForceInfiniteRejectPublish bool
}

/********************************* BROKER *********************************/

// Broker implements a simple way to consume & publish with rabbitmq.
// It will handle automatically everything from retries to reconnections.
type Broker struct {
	consumer *consumer.RabbitMQConsumer
	producer *producer.RabbitMQProducer
	tools    *tools.Tools
	errChan  chan error

	reConn common.ConnReconnector
	closed chan struct{}
}

/*
New creates a new broker.
The context given will only be used for closing:

If context is closed, all consuming will stop, BUT nothing will disconnect
(production & ack/nack still possible).
To properly disconnect, call [Broker.Close].
To wait for all ongoing publish to finish before closing,
call [Broker.Wait] before [Broker.Close]
*/
func New(ctx context.Context, conf Conf) *Broker {
	b := Broker{
		errChan: make(chan error, 1),
		closed:  make(chan struct{}),
	}

	b.reConn = connection.NewReconnector(conf.URI, conf.ConnName, b.errChan)

	b.tools = tools.NewTools(b.reConn)

	if conf.Consumer != nil {
		b.consumer = consumer.NewRabbitMQConsumer(
			ctx,
			b.reConn,
			consumer.Conf{
				ChannelPoolSize: conf.Consumer.ChannelPoolSize,
				QOS:             conf.Consumer.QOS,
			},
		)
	}

	if conf.Producer != nil {
		b.producer = producer.NewRabbitMQProducer(
			ctx,
			b.reConn,
			producer.Conf{
				ChannelPoolSize:     conf.Producer.ChannelPoolSize,
				ConfirmationTimeout: conf.Producer.ConfirmationTimeout,
				ForcePersistence:    conf.Producer.ForcePersistence,
				ForceMandatory:      conf.Producer.ForceMandatory,
			},
		)
	}

	return &b
}

/*
Consume returns consumer deliveries channel on specified queue.

If a disconnection occures, reconnection is automatic and invisible.
If an error occures when reconnecting, the error will be sent to the output of [Broker.ErrChan] (only for logs).
In case of reconnection error for more than 1 minute, consumption stops (chan close).

If the consumer stops, it is the caller's responsibility to know if it's from
a manual [Broker.Cancel], a ctx close or an error.

The tag must be unique.
Using the same tag than a running consumer will lead to unpredictable behaviours.
Using a different tag with the same bindings will register one more consumer on the same queue.

Use [Broker.Cancel](tag) to stop  a specific queue consuming without closing connections.
cancel the broker's context to stop all consumptions.
In both cases, the go chans returned by all past [Broker.Consume] will close, but the amqp connection will remain open:
[Delivery.Ack] calls are still possible.

if options is nil: autoAck, exclusive and noWait default to false.
Otherwise, use [NewConsumeOptions] to create it.

In case of internal reconnection some message [Delivery.Ack] can return an [amqp091.ErrClosed] error.
You can continue running normally in that case; otherwise, it is advised to retry [Delivery.Ack] or crash.

This method can return an error true to
[gitlab.com/qosenergy/amqpw/error.IsErrRetriable].
This means the connection failed
but could be retried. If the error is false, it implies some other issue: you should raise an error.

The returned error will be io.EOF if the context of the broker is closed.

[Delivery.Ack]: https://pkg.go.dev/github.com/rabbitmq/amqp091-go#Delivery.Ack
[amqp091.ErrClosed]: https://pkg.go.dev/github.com/rabbitmq/amqp091-go#pkg-variables
*/
func (b *Broker) Consume(
	queue string, tag string, options *ConsumeOptions,
) (<-chan amqp091.Delivery, error) {
	if b.consumer == nil {
		return nil, fmt.Errorf("Consume() while no consumer conf was provided")
	}
	var consumeOptions *consumer.ConsumeOptions
	if options != nil {
		consumeOptions = &options.ConsumeOptions
	}
	return b.consumer.Consume(queue, tag, consumeOptions)
}

/*
Cancel tries canceling the consumption for the given tag.
In case of success, the chan returned by [Broker.Consume] with this tag will be closed once all remaining
prefetched messages are consumed.

If a Retriable error is returned, a network error occurred and the consumer was not cancelled.
If it is not Retriable, then there was no active consumer with this tag.
*/
func (b *Broker) Cancel(
	tag string,
) error {
	if b.consumer == nil {
		return fmt.Errorf("Cancel() while no consumer conf was provided")
	}
	return b.consumer.Cancel(tag)
}

/*
Publish synchronously sends a message to an exchange which must have been created before.
If not, publish will fail.
It returns a nil error once write is confirmed by the server or a confirmation error otherwise.

The returned error can be checked with [gitlab.com/qosenergy/amqpw/error.IsErrRetriable]
to check retriability, but it is advised to use
[Broker.PublishWithRetry] or [Broker.PublishASyncWithRetry] funcs instead.

The returned error can be [io.EOF] if [Broker.Close] was called beforehand (producer is definitely unusable then)

The returned error can be true to [gitlab.com/qosenergy/amqpw/error.IsErrPublishRejected] if:
  - one output queue is full with reject-publish
  - mandatory is on and the message could not be published to any queue
*/
func (b *Broker) Publish(
	exchange, routingKey string,
	publishing amqp091.Publishing,
	options *ProduceOptions,
) error {
	if b.producer == nil {
		return fmt.Errorf("Publish() while no producer conf was provided")
	}
	var produceOptions *producer.ProduceOptions
	if options != nil {
		produceOptions = &options.ProduceOptions
	}
	return b.producer.Publish(exchange, routingKey, publishing, produceOptions)
}

/*
PublishASync works the same as Publish but returns before receiving confirmation from RabbitMQ.
The callback will be called once the publishing confirmed or expired.

The callback error can be checked with amqpwerror.IsErrRetriable to check retriability,
but it advised to use publish*WithRetry funcs instead.

The error sent to callback can be [io.EOF] if [Broker.Close]
was called beforehand (producer is definitely unusable then)

A nil error with false ack implies a reject publish, it happens if:
  - one output queue is full with reject-publish
  - mandatory is on and the message could not be published to any queue

FYI: this function becomes blocking if more than 1000 messages are currently being published.
This should not happen unless you have connection issues or rabbitMQ is unstable.
As a side effect, there is no need to restrain call to ASync Publishes on your side.
*/
func (b *Broker) PublishASync(
	exchange, routingKey string,
	publishing amqp091.Publishing,
	callback func(ack bool, err error),
	options *ProduceOptions,
) {
	if b.producer == nil {
		go callback(false, fmt.Errorf("PublishASync() while no producer conf was provided"))
	}
	var produceOptions *producer.ProduceOptions
	if options != nil {
		produceOptions = &options.ProduceOptions
	}
	b.producer.PublishASync(exchange, routingKey, publishing, callback, produceOptions)
}

/*
PublishWithRetry calls [Broker.Publish] until it succeeds or the number of retries defined
in the backoff strategy is reached.

For every retry, the cause of failure of the previous try is sent to statusChan if not nil AND not full.
This exists exclusively to give context to multiple retries occurring, not to do specific error management.

statusChan is never closed by the broker as you could have a statusChan
per published message or a common statusChan for all.
*/
func (b *Broker) PublishWithRetry(
	exchange, routingKey string,
	publishing amqp091.Publishing,
	retryBackoff backoff.BackOff,
	statusChan chan error,
	options *ProduceOptions,
) error {
	if b.producer == nil {
		return fmt.Errorf("PublishWithRetry() while no producer conf was provided")
	}
	var produceOptions *producer.ProduceOptions
	if options != nil {
		produceOptions = &options.ProduceOptions
	}
	return b.producer.PublishWithRetry(exchange, routingKey, publishing, retryBackoff, statusChan, produceOptions)
}

/*
PublishASyncWithRetry calls [Broker.PublishASync] until it succeeds to publish and the delivery has been confirmed
or if the number of retries defined in the backoff strategy is reached.

For every retry, the cause of failure of the previous try is sent to statusChan if not nil AND not full.
This exists exclusively to give context to multiple retries occurring, not to do specific error management.
*/
func (b *Broker) PublishASyncWithRetry(
	exchange, routingKey string,
	publishing amqp091.Publishing,
	callback func(ack bool, err error),
	retryBackoff backoff.BackOff,
	statusChan chan error,
	options *ProduceOptions,
) {
	if b.producer == nil {
		go callback(false, fmt.Errorf("PublishASyncWithRetry() while no producer conf was provided"))
	}
	var produceOptions *producer.ProduceOptions
	if options != nil {
		produceOptions = &options.ProduceOptions
	}
	b.producer.PublishASyncWithRetry(
		exchange, routingKey, publishing, callback, retryBackoff, statusChan, produceOptions,
	)
}

// Wait blocks until all messages in the producer either timeouts or receives ack/unack.
// Calling this while still producing could block forever.
// It is advised to call this before [Broker.Close] for clean shutdown.
func (b *Broker) Wait() {
	b.producer.Wait()
}

/*
Close sets the producer to close itself, waits for it to finish, then disconnects everything.
Once This has been called, publishing and Acking messages becomes impossible.
Close is not thread safe, and should not be called twice.

It is advised to call [Broker.Wait] beforehand to only disconnect once all ongoing work is done.

It also closes the output of [Broker.ErrChan].
*/
func (b *Broker) Close() error {
	select {
	case <-b.closed:
		return nil
	default:
		close(b.closed)
	}

	var err error
	if b.producer != nil {
		err = errors.Join(err, common.NotNilWrap("close producer: %w", b.producer.Close()))
	}
	err = errors.Join(err, common.NotNilWrap("connection close: %w", b.reConn.Close()))
	close(b.errChan)
	return common.NotNilWrap("closing broker: [\n%w\n]", err)
}

// ErrChan returns a chan in which all internal async error will be sent.
// This is for logging purposes as those errors are already handled internally.
// Not reading this chan will NOT lead to blocking the broker.
// Errors specific to a published message with retries will be sent to its statusChan instead.
func (b *Broker) ErrChan() <-chan error {
	return b.errChan
}

// CheckQueue returns "true & some metadata about the queue" if it exists.
// It returns "false & nil" if it does not exist.
// It returns an error if the conf is invalid or if some network error occurred.
func (b *Broker) CheckQueue(queueName string) (bool, *amqp091.Queue, error) {
	return b.tools.CheckQueue(queueName)
}

// CheckExchange returns true if the exchange exists.
// It returns false if it does not exist.
// It returns an error if the conf is invalid or if some network error occurred.
func (b *Broker) CheckExchange(exchangeName string) (bool, error) {
	return b.tools.CheckExchange(exchangeName)
}
