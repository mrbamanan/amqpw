// Package channel handles amqp channel auto reconnection
package channel

import (
	"context"
	"errors"
	"fmt"
	"io"
	"sync"

	"github.com/rabbitmq/amqp091-go"

	"gitlab.com/qosenergy/amqpw/internal/common"
)

// Reconnector wraps a channel to give the existing channel if is connected or create a new one if not.
// Also reconnects amqp connection if need be (handled by amqpwconn.Reconnector).
// It is thread-safe
type Reconnector struct {
	reconn   common.ConnReconnector
	currChan common.Channel

	mx       sync.Mutex
	wg       sync.WaitGroup
	isClosed bool
}

// NewReconnector creates a new Reconnector
func NewReconnector(reconn common.ConnReconnector) common.ChannelReconnector {
	return &Reconnector{
		reconn: reconn,
	}
}

// Channel returns the existing channel if it is connected & returnes a new connected one otherwise
// The channel will not be initialized until this func is called for the first time.
// auto reconnection is included.
// If this Reconnector or the parent connection Reconnector was closed manually (func Close()),
// this function returns the error io.EOF (use errors.Is to check).
func (rch *Reconnector) Channel(qos *common.QOS, confirm bool) (common.Channel, error) {
	rch.mx.Lock()
	defer rch.mx.Unlock()
	if rch.isClosed {
		return nil, io.EOF
	}
	if rch.currChan != nil {
		return rch.currChan, nil
	}
	conn, connErr := rch.reconn.Connection()
	if connErr != nil {
		if errors.Is(connErr, io.EOF) {
			rch.isClosed = true
			return rch.currChan, io.EOF
		}
		return nil, fmt.Errorf("amqp Connection for Channel: %w", connErr)
	}

	currChan, chanErr := conn.Channel()
	if chanErr != nil {
		return nil, common.SetRetriableErr(fmt.Errorf("amqp new Channel: %w", chanErr))
	}

	if confirm {
		confirmErr := currChan.Confirm(false)
		if confirmErr != nil {
			return nil, common.SetRetriableErr(fmt.Errorf("confirm mode: %w", confirmErr))
		}
	}

	if qos != nil {
		qosErr := currChan.Qos(qos.PrefetchCount, qos.PrefetchSize, qos.Global)
		if qosErr != nil {
			return nil, common.SetRetriableErr(fmt.Errorf("applying QOS on new channel: %w", qosErr))
		}
	}

	rch.currChan = currChan

	var closeChan = make(chan *amqp091.Error)
	closeChan = rch.currChan.NotifyClose(closeChan)
	rch.wg.Add(1)
	go rch.listenClose(closeChan)

	return rch.currChan, nil
}

func (rch *Reconnector) listenClose(errChan <-chan *amqp091.Error) {
	closeErr := <-errChan

	rch.mx.Lock()
	defer rch.mx.Unlock()

	rch.currChan = nil

	defer rch.wg.Done()

	if closeErr == nil {
		return
	}

	rch.SendErr(fmt.Errorf("channel: listenClose: %w", closeErr))
}

// Close closes the channel reconnector and stops it from reconnecting further.
// If parent connection is manually closed, there is no need to call this.
func (rch *Reconnector) Close() error {
	rch.mx.Lock()
	rch.isClosed = true
	if rch.currChan == nil {
		return nil
	}
	currChan := rch.currChan
	rch.currChan = nil
	rch.mx.Unlock()

	closeErr := currChan.Close()
	if closeErr != nil {
		return fmt.Errorf("closing channel: %w", closeErr)
	}

	rch.wg.Wait()

	return nil
}

// SendErr is used internally by amqpw to return errors to log to a common chan.
// If the error chan is not consumed, this is non blocking.
func (rch *Reconnector) SendErr(err error) {
	rch.reconn.SendErr(err)
}

// deferredConfirmationImpl wraps amqp091.DeferredConfirmation so it satisfies DeferredConfirmation interface
type deferredConfirmationImpl struct {
	*amqp091.DeferredConfirmation
}

// DeliveryTag return the DeliveryTag of the deferredConfirmation
func (dc deferredConfirmationImpl) DeliveryTag() uint64 {
	return dc.DeferredConfirmation.DeliveryTag
}

// ChanWrap is the same as amqp091.Channel with a New signature for
// PublishWithDeferredConfirmWithContext to make it mockable
type ChanWrap struct {
	*amqp091.Channel
}

/*
PublishWithDeferredConfirmWithContext sends a Publishing from the client to an exchange on the server.

It additionally returns a DeferredConfirmation, allowing the caller to wait on the publisher
confirmation for this message. If the channel has not been put into confirm mode,
the DeferredConfirmation will be nil.

When you want a single message to be delivered to a single queue, you can
publish to the default exchange with the routingKey of the queue name.  This is
because every declared queue gets an implicit route to the default exchange.

Since publishings are asynchronous, any undeliverable message will get returned
by the server.  Add a listener with Channel.NotifyReturn to handle any
undeliverable message when calling publish with either the mandatory or
immediate parameters as true.

Publishings can be undeliverable when the mandatory flag is true and no queue is
bound that matches the routing key, or when the immediate flag is true and no
consumer on the matched queue is ready to accept the delivery.

This can return an error when the channel, connection or socket is closed.  The
error or lack of an error does not indicate whether the server has received this
publishing.

It is possible for publishing to not reach the broker if the underlying socket
is shut down without pending publishing packets being flushed from the kernel
buffers.  The easy way of making it probable that all publishings reach the
server is to always call Connection.Close before terminating your publishing
application.  The way to ensure that all publishings reach the server is to add
a listener to Channel.NotifyPublish and put the channel in confirm mode with
Channel.Confirm.  Publishing delivery tags and their corresponding
confirmations start at 1.  Exit when all publishings are confirmed.

When Publish does not return an error and the channel is in confirm mode, the
internal counter for DeliveryTags with the first confirmation starts at 1.
*/
func (cw *ChanWrap) PublishWithDeferredConfirmWithContext(
	ctx context.Context,
	exchange, routingKey string,
	mandatory, immediate bool,
	msg amqp091.Publishing,
) (common.DeferredConfirmation, error) {
	deferredConfirm, pubErr := cw.Channel.PublishWithDeferredConfirmWithContext(
		ctx, exchange, routingKey, mandatory, immediate, msg,
	)
	if pubErr != nil {
		return nil, pubErr
	}
	return deferredConfirmationImpl{DeferredConfirmation: deferredConfirm}, nil
}
