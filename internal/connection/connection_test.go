package connection

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/rabbitmq/amqp091-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/qosenergy/amqpw/internal/common"
)

func TestNewReconnector(t *testing.T) {
	t.Run("simple test NewReconnector", func(t *testing.T) {
		uri := "testURI"
		var errChan chan<- error = make(chan error)

		reconnector := NewReconnector(uri, "test conn", errChan)

		castReconnector, ok := reconnector.(*Reconnector)
		if assert.True(t, ok, "result is a (*Reconnector)") {
			assert.Equal(t, castReconnector.URI, uri, "uri set OK")
			assert.Equal(t, castReconnector.errChan, errChan, "errChan properly set")
			assert.NotNil(t, castReconnector.dial, "dial func initialized")
		}
	})
}

func TestSendErr(t *testing.T) {
	// we do this test fully synchrone to simulate reading errChan being late
	type test struct {
		errChan    chan error
		sendNB     int
		expectedNB int
	}

	tests := map[string]test{
		"all messages go through": {
			errChan:    make(chan error, 5),
			sendNB:     5,
			expectedNB: 5,
		},
		"some messages can't go through": {
			errChan:    make(chan error, 2),
			sendNB:     5,
			expectedNB: 2,
		},
		"nil chan": {
			sendNB:     5,
			expectedNB: 0,
		},
	}

	for label, tt := range tests {
		t.Run(label, func(t *testing.T) {
			assertions := assert.New(t)

			reconnector := &Reconnector{errChan: tt.errChan}
			timeoutErr := fmt.Errorf("timeout")
			ctx, cancel := context.WithTimeoutCause(context.Background(), time.Second, timeoutErr)
			// send
			go func() {
				for i := 0; i < tt.sendNB; i++ {
					reconnector.SendErr(fmt.Errorf("boarf"))
				}
				cancel()
			}()
			// wait send done
			<-ctx.Done()
			// check no timeout
			assertions.Equal(context.Cause(ctx), context.Canceled, "non blocking")
			// check result
			var msgCount int
			if tt.errChan != nil {
				close(tt.errChan)
				for range tt.errChan {
					msgCount++
				}
			}
			assertions.Equal(msgCount, tt.expectedNB, "received errors OK")
		})
	}
}

func TestClose(t *testing.T) {
	type test struct {
		newReConn   func(*testing.T) *Reconnector
		wait        bool
		expectedErr string
	}

	tests := map[string]test{
		"already closed": {
			newReConn: func(*testing.T) *Reconnector {
				return &Reconnector{
					isClosed: true,
				}
			},
			expectedErr: "",
		},
		"currently not connected (not init or disconnected)": {
			newReConn: func(*testing.T) *Reconnector {
				return &Reconnector{
					currConn: nil,
				}
			},
			expectedErr: "",
		},
		"close OK": {
			newReConn: func(t *testing.T) *Reconnector {
				mConn := common.NewMockConnection(t)
				mConn.On("Close").Return(nil).Once()
				return &Reconnector{
					currConn: mConn,
				}
			},
			expectedErr: "",
		},
		"close KO": {
			newReConn: func(t *testing.T) *Reconnector {
				mConn := common.NewMockConnection(t)
				mConn.On("Close").Return(fmt.Errorf("close fail")).Once()
				return &Reconnector{
					currConn: mConn,
				}
			},
			expectedErr: "close fail",
		},

		"close wait OK": {
			newReConn: func(t *testing.T) *Reconnector {
				mConn := common.NewMockConnection(t)
				mConn.On("Close").Return(nil).Once()
				reco := &Reconnector{
					currConn: mConn,
				}
				reco.wg.Add(1)
				return reco
			},
			expectedErr: "",
			wait:        true,
		},
	}

	for label, tt := range tests {
		t.Run(label, func(t *testing.T) {
			reconn := tt.newReConn(t)

			ctx, cancel := context.WithTimeoutCause(context.Background(), 2*time.Second, fmt.Errorf("timeout"))
			var closeErr error
			go func() {
				closeErr = reconn.Close()
				cancel()
			}()
			if tt.wait {
				time.Sleep(10 * time.Millisecond)
				select {
				case <-ctx.Done():
					assert.Fail(t, "close finish while we expected it to wait")
				default:
				}
				reconn.wg.Done()
			}
			<-ctx.Done()
			if assert.Equal(t, context.Cause(ctx), context.Canceled, "close returned") {
				if tt.expectedErr == "" {
					assert.NoError(t, closeErr, "no error expected")
				} else if assert.Error(t, closeErr, "error expected") {
					assert.Contains(t, closeErr.Error(), tt.expectedErr, "error content OK")
				}
			}
		})
	}
}

type timeoutError struct{}

func (*timeoutError) Error() string   { return "i/o timeout" }
func (*timeoutError) Timeout() bool   { return true }
func (*timeoutError) Temporary() bool { return true }

func TestConnection(t *testing.T) {
	type test struct {
		newReConn   func(*testing.T) (*Reconnector, *common.MockConnection, chan *amqp091.Error)
		expectedErr string
	}

	tests := map[string]test{
		"new/re conn OK": {
			newReConn: func(t *testing.T) (*Reconnector, *common.MockConnection, chan *amqp091.Error) {
				mockConn := common.NewMockConnection(t)
				notifClose := make(chan *amqp091.Error)
				mockConn.
					On("NotifyClose", mock.Anything).
					Return(notifClose).
					Once()

				return &Reconnector{
					currConn: nil,
					URI:      "dummy",
					dial: func(string) (common.Connection, error) {
						return mockConn, nil
					},
				}, mockConn, notifClose
			},
		},
		"closed => io.EOF": {
			newReConn: func(*testing.T) (*Reconnector, *common.MockConnection, chan *amqp091.Error) {
				return &Reconnector{
					currConn: nil,
					URI:      "dummy",
					isClosed: true,
				}, nil, nil
			},
			expectedErr: "EOF",
		},
		"already connected": {
			newReConn: func(*testing.T) (*Reconnector, *common.MockConnection, chan *amqp091.Error) {
				var mockConn common.MockConnection

				return &Reconnector{
					currConn: &mockConn,
					URI:      "dummy",
					dial: func(string) (common.Connection, error) {
						return &mockConn, nil
					},
				}, &mockConn, nil
			},
		},
		"new/re conn error": {
			newReConn: func(*testing.T) (*Reconnector, *common.MockConnection, chan *amqp091.Error) {
				return &Reconnector{
					currConn: nil,
					URI:      "dummy",
					dial: func(string) (common.Connection, error) {
						return nil, fmt.Errorf("conn error")
					},
				}, nil, nil
			},
			expectedErr: "conn error",
		},
		"new/re conn error: timeout": {
			newReConn: func(*testing.T) (*Reconnector, *common.MockConnection, chan *amqp091.Error) {
				return &Reconnector{
					currConn: nil,
					URI:      "dummy",
					dial: func(string) (common.Connection, error) {
						return nil, &timeoutError{}
					},
				}, nil, nil
			},
			expectedErr: "i/o timeout",
		},
	}

	for label, tt := range tests {
		t.Run(label, func(t *testing.T) {
			reconn, expectedMockConn, notifCloseChan := tt.newReConn(t)

			resConn, err := reconn.Connection()
			if tt.expectedErr != "" {
				if assert.Error(t, err, "expect error") {
					assert.Contains(t, err.Error(), tt.expectedErr)
				}
				return
			}

			if resMockConn, ok := resConn.(*common.MockConnection); assert.True(t, ok, "return conn type OK") {
				assert.Equal(t, resMockConn, expectedMockConn, "result conn OK")
			}
			// test closing
			if notifCloseChan != nil {
				notifCloseChan <- amqp091.ErrClosed
				reconn.wg.Wait()
				assert.Nil(t, reconn.currConn, "conn nil after notifClose")
			}
		})
	}
}
