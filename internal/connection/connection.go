// Package connection defines the low level reconnection system
package connection

import (
	"errors"
	"fmt"
	"io"
	"net"
	"sync"

	"github.com/rabbitmq/amqp091-go"

	"gitlab.com/qosenergy/amqpw/internal/common"
	"gitlab.com/qosenergy/amqpw/internal/connection/channel"
)

// Reconnector wraps a connection to give the existing connection if is connected or create a new one if not.
// It is thread-safe
type Reconnector struct {
	currConn common.Connection
	URI      string

	mx       sync.Mutex
	isClosed bool
	wg       sync.WaitGroup
	errChan  chan<- error
	// dial is replacable for tests
	dial func(URI string) (common.Connection, error)
}

// NewReconnector creates a new Reconnector
func NewReconnector(amqpURI, connName string, errChan chan<- error) common.ConnReconnector {
	return &Reconnector{
		URI:     amqpURI,
		errChan: errChan,
		dial: func(URI string) (common.Connection, error) {
			newConn, connErr := amqp091.DialConfig(URI, dialConf(connName))
			conn := &connWrap{
				Connection: newConn,
			}
			return conn, connErr
		},
	}
}

func dialConf(connectionName string) amqp091.Config {
	table := amqp091.Table{}
	table.SetClientConnectionName(connectionName)
	return amqp091.Config{Properties: table}
}

// Connection returns the existing connection if it is connected & returnes a new one otherwise
// The connecion will not be initialized until this func is called for the first time.
// If this Reconnector was closed manually (func Close()),
// this function returns the error io.EOF (use errors.Is to check).
func (rco *Reconnector) Connection() (common.Connection, error) {
	rco.mx.Lock()
	defer rco.mx.Unlock()
	if rco.isClosed {
		return nil, io.EOF
	}
	if rco.currConn != nil {
		return rco.currConn, nil
	}

	currConn, connErr := rco.dial(rco.URI)
	if connErr != nil {
		var netErr net.Error
		if errors.As(connErr, &netErr) && (netErr).Timeout() {
			connErr = common.SetRetriableErr(connErr)
		}
		return currConn, fmt.Errorf("connecting: %w", connErr)
	}
	rco.currConn = currConn
	var closeChan = make(chan *amqp091.Error)
	closeChan = rco.currConn.NotifyClose(closeChan)
	rco.wg.Add(1)
	go rco.listenClose(closeChan)

	return rco.currConn, nil
}

// Close closes the connection reconnector and stops it from reconnecting further.
// It also closes all channel & channel reconnectors descending from it
func (rco *Reconnector) Close() error {
	rco.mx.Lock()
	locked := true
	defer func() {
		if locked {
			rco.mx.Unlock()
		}
	}()
	if rco.isClosed {
		return nil
	}

	rco.isClosed = true

	if rco.currConn == nil {
		return nil
	}
	closeErr := rco.currConn.Close()
	if closeErr != nil {
		return fmt.Errorf("closing amqp Connection: %w", closeErr)
	}

	locked = false
	rco.mx.Unlock()
	rco.wg.Wait()

	// errChan should be closed, make very sure we do not try to write to it anymore
	rco.mx.Lock()
	rco.errChan = nil
	rco.mx.Unlock()

	return nil
}

// SendErr is used internally by amqpw to return errors to log to a common chan.
// If the error chan is not consumed, this is non blocking.
func (rco *Reconnector) SendErr(err error) {
	// avoid writing to close chan from channel listenClose()
	rco.mx.Lock()
	defer rco.mx.Unlock()
	select {
	case rco.errChan <- err:
	default:
	}
}

// NewReChannel creates a new ChannelReconnector from the given connection Reconnector
// It serves mainly to simplify mocking procedure for those
func (rco *Reconnector) NewReChannel() common.ChannelReconnector {
	return channel.NewReconnector(rco)
}

func (rco *Reconnector) listenClose(closeChan <-chan *amqp091.Error) {
	closeErr := <-closeChan

	rco.mx.Lock()

	rco.currConn = nil

	defer rco.wg.Done()

	rco.mx.Unlock()
	if closeErr == nil {
		return
	}
	rco.SendErr(fmt.Errorf("connection: listenClose: %w", closeErr))
}

// connWrap is the same as amqp091.Connection with a New signature for Channel() to make it mockable
type connWrap struct {
	*amqp091.Connection
}

func (cw *connWrap) Channel() (common.Channel, error) {
	ch, chErr := cw.Connection.Channel()
	if chErr != nil {
		return nil, chErr
	}
	return &channel.ChanWrap{Channel: ch}, nil
}
