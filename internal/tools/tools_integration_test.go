//go:build integration

package tools

import (
	"fmt"
	"os"
	"testing"

	"github.com/rabbitmq/amqp091-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/qosenergy/amqpw/internal/connection"
	"gitlab.com/qosenergy/amqpw/internal/test"
)

func TestCheckQueue(t *testing.T) {
	assertion := require.New(t)

	uri, ok := os.LookupEnv(test.AmqpURIKey)
	assertion.True(ok, fmt.Sprintf("%q env var missing", test.AmqpURIKey))

	exchange := "test_exchange"
	queue := "test_queue"
	err := test.ExchangeDeclare(uri, exchange, amqp091.ExchangeTopic)
	assertion.NoError(err, "init exchange")
	err = test.QueueDeclare(uri, queue)
	assertion.NoError(err, "init queue")

	t.Run("existing exchange", func(t *testing.T) {
		assertion := assert.New(t)
		rco := connection.NewReconnector(uri, "existing exchange test", nil)
		checkTools := NewTools(rco)
		ok, err := checkTools.CheckExchange(exchange)
		assertion.NoError(err, "no error")
		assertion.True(ok, "exchange exits")
		assertion.NoError(rco.Close(), "close conn")
	})

	t.Run("non existing exchange", func(t *testing.T) {
		assertion := assert.New(t)
		rco := connection.NewReconnector(uri, "non existing exchange test", nil)
		checkTools := NewTools(rco)
		ok, err := checkTools.CheckExchange("non_existant")

		assertion.NoError(err, "no error")
		assertion.False(ok, "exchange does not exit")
		assertion.NoError(rco.Close(), "close conn")
	})

	t.Run("existing queue", func(t *testing.T) {
		assertion := assert.New(t)
		rco := connection.NewReconnector(uri, "existing queue test", nil)
		checkTools := NewTools(rco)
		ok, q, err := checkTools.CheckQueue(queue)
		assertion.NoError(err, "no error")
		assertion.True(ok, "queue exits")
		if assertion.NotNil(q.Name, "amqp091.Queue not nil") {
			assertion.Equal(q.Name, queue, "resturned queue is the right one")
		}
		assertion.NoError(rco.Close(), "close conn")
	})

	t.Run("non existing queue", func(t *testing.T) {
		assertion := assert.New(t)
		rco := connection.NewReconnector(uri, "non existing queue test", nil)
		checkTools := NewTools(rco)
		ok, q, err := checkTools.CheckQueue("non_existant")
		assertion.NoError(err, "no error")
		assertion.False(ok, "exchange does not exit")
		assertion.Nil(q, "amqp091.Queue nil")
		assertion.NoError(rco.Close(), "close conn")
	})
}
