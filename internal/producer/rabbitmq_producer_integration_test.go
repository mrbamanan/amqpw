//go:build integration

package producer

import (
	"context"
	"fmt"
	"os"
	"testing"

	"github.com/rabbitmq/amqp091-go"
	"github.com/stretchr/testify/require"
	"gitlab.com/qosenergy/amqpw/internal/connection"
	"gitlab.com/qosenergy/amqpw/internal/test"
)

func TestRabbitMQConsumer_Consume(t *testing.T) {
	assertion := require.New(t)

	uri, ok := os.LookupEnv(test.AmqpURIKey)
	assertion.True(ok, fmt.Sprintf("%q env var missing", test.AmqpURIKey))

	queue := "q.test"
	body := []byte("test")

	rco := connection.NewReconnector(uri, "test consumer conn", nil)

	prodsr := NewRabbitMQProducer(context.Background(), rco, Conf{})

	err := prodsr.Publish(
		"", queue,
		amqp091.Publishing{Body: body},
		nil,
	)
	assertion.NoError(err, "publish")

	msg, err := test.Consume(uri, queue)
	assertion.NoError(err, "consume")

	assertion.Equal(body, msg.Body, "producing message")
	assertion.NoError(prodsr.Close(), "close producer")
	assertion.NoError(rco.Close(), "close conn")
}
