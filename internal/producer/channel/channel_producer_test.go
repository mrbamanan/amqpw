package channel

// revive:disable:dot-imports

import (
	"context"
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/rabbitmq/amqp091-go"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"golang.org/x/sync/errgroup"

	"gitlab.com/qosenergy/amqpw/internal/common"
	"gitlab.com/qosenergy/amqpw/internal/producer/smartmessage"
	"gitlab.com/qosenergy/amqpw/internal/producer/smartmessage/event"
	"gitlab.com/qosenergy/amqpw/internal/test"
)

// revive:enable:dot-imports

func TestNewProducer(t *testing.T) {
	Convey("on basic NewProducer() call", t, func(c C) {
		cmc := test.NewConveyMockCleanup(t, c)
		Reset(func() { cmc.Clean() })
		chanReconn := common.NewMockChannelReconnector(cmc)
		producer := NewProducer(
			context.Background(),
			chanReconn,
			(<-chan smartmessage.SmartPublish)(nil),
		)
		SoMsg("channel producer properly created", producer, ShouldNotBeNil)
		SoMsg("inner confirmationHandler properly created", producer.confirmationHandler, ShouldNotBeNil)
	})
}

func TestStart(t *testing.T) {
	Convey("when we publish a message on a started channel producer", t, func(c C) {
		cmc := test.NewConveyMockCleanup(t, c)
		Reset(func() { cmc.Clean() })
		mockChanReconn := common.NewMockChannelReconnector(cmc)
		queryChan := make(chan smartmessage.SmartPublish, 1)
		cancellableCtx, cancel := context.WithCancel(context.Background())
		t.Cleanup(func() { cancel() })
		errGrp, ctx := errgroup.WithContext(cancellableCtx)
		producer := NewProducer(
			ctx,
			mockChanReconn,
			queryChan,
		)
		producer.Start(errGrp)
		mockMsg := smartmessage.NewMockSmartPublish(cmc)

		Convey("but channel does not connect with non retriable error", func() {
			var wg sync.WaitGroup
			mockChanReconn.
				On("Channel", (*common.QOS)(nil), true).
				Return((common.Channel)(nil), fmt.Errorf("oops")).
				Once()
			mockChanReconn.On("SendErr", mock.Anything).Once()
			mockMsg.
				On("Callback", false, mock.Anything).
				Run(func(mock.Arguments) { wg.Done() }).
				Once()

			// submit msg
			wg.Add(1)
			queryChan <- mockMsg

			// wait callback
			wgOk := make(chan struct{})
			go func() {
				wg.Wait()
				close(wgOk)
			}()
			testTO := time.NewTimer(time.Second)
			var ok bool
			select {
			case <-testTO.C:
			case <-wgOk:
				ok = true
			}
			SoMsg("everything runs as expected", ok, ShouldBeTrue)
		})
		Convey("but channel does not connect with retriable error", func() {
			var wg sync.WaitGroup
			mockChanReconn.
				On("Channel", (*common.QOS)(nil), true).
				Return((common.Channel)(nil), common.SetRetriableErr(fmt.Errorf("oops"))).
				Once()
			mockChanReconn.On("SendErr", mock.Anything).Once()
			mockMsg.
				On("SubmitRetryBackOffEvent", mock.Anything).
				Run(func(mock.Arguments) { wg.Done() }).
				Once()

			// submit msg
			wg.Add(1)
			queryChan <- mockMsg

			// wait retry
			wgOk := make(chan struct{})
			go func() {
				wg.Wait()
				close(wgOk)
			}()
			testTO := time.NewTimer(time.Second)
			var ok bool
			select {
			case <-testTO.C:
			case <-wgOk:
				ok = true
			}
			SoMsg("everything runs as expected", ok, ShouldBeTrue)
		})
		Convey("but generic publish error occures", func() {
			var wg sync.WaitGroup
			mockChannel := common.NewMockChannel(cmc)
			mockChanReconn.
				On("Channel", (*common.QOS)(nil), true).
				Return(mockChannel, (error)(nil)).
				Once()
			mockMsg.
				On("Publish", ctx, mockChannel).
				Return((common.DeferredConfirmation)(nil), fmt.Errorf("publish err")).
				Once()

			mockMsg.
				On("SubmitRetryBackOffEvent", mock.Anything).
				Run(func(mock.Arguments) { wg.Done() }).
				Once()

			// submit msg
			wg.Add(1)
			queryChan <- mockMsg

			// wait retry
			wgOk := make(chan struct{})
			go func() {
				wg.Wait()
				close(wgOk)
			}()
			testTO := time.NewTimer(time.Second)
			var ok bool
			select {
			case <-testTO.C:
			case <-wgOk:
				ok = true
			}
			SoMsg("everything runs as expected", ok, ShouldBeTrue)
		})
		Convey("but Close publish error occures", func() {
			var wg sync.WaitGroup
			mockChannel := common.NewMockChannel(cmc)
			mockChanReconn.
				On("Channel", (*common.QOS)(nil), true).
				Return(mockChannel, (error)(nil)).
				Once()
			mockMsg.
				On("Publish", ctx, mockChannel).
				Return((common.DeferredConfirmation)(nil),
					fmt.Errorf("publish err: %w", amqp091.ErrClosed),
				).
				Once()
			mockMsg.
				On("SubmitForPublish").
				Run(func(mock.Arguments) { wg.Done() }).
				Once()

			// submit msg
			wg.Add(1)
			queryChan <- mockMsg

			// wait retry
			wgOk := make(chan struct{})
			go func() {
				wg.Wait()
				close(wgOk)
			}()
			testTO := time.NewTimer(time.Second)
			var ok bool
			select {
			case <-testTO.C:
			case <-wgOk:
				ok = true
			}
			SoMsg("everything runs as expected", ok, ShouldBeTrue)
		})
		Convey("and all goes well", func() {
			var wg sync.WaitGroup
			mockDeferredConfirm := common.NewMockDeferredConfirmation(cmc)
			mockDeferredConfirm.On("DeliveryTag").Return(uint64(2))
			doneChan := make(chan struct{})
			mockDeferredConfirm.On("Done").Return((<-chan struct{})(doneChan)).Once()
			mockChannel := common.NewMockChannel(cmc)
			mockChanReconn.
				On("Channel", (*common.QOS)(nil), true).
				Return(mockChannel, (error)(nil)).
				Once()
			mockMsg.
				On("Publish", ctx, mockChannel).
				Return(mockDeferredConfirm, (error)(nil)).
				Once()
			mockMsg.
				On("SubmitTOEvent", producer.confirmationHandler, mockDeferredConfirm.DeliveryTag()).
				Return(
					event.NewTOEvent(
						time.Now().Add(time.Second), 2,
						producer.confirmationHandler, mockMsg,
					),
				).
				Once()

			// submit msg
			wg.Add(1)
			queryChan <- mockMsg

			mockDeferredConfirm.On("Acked").Return(true).Once()
			mockMsg.
				On("Callback", true, (error)(nil)).
				Run(func(mock.Arguments) { wg.Done() }).
				Once()

			// mark message as published
			close(doneChan)
			// wait callback
			wgOk := make(chan struct{})
			go func() {
				wg.Wait()
				close(wgOk)
			}()
			testTO := time.NewTimer(time.Second)
			var ok bool
			select {
			case <-testTO.C:
			case <-wgOk:
				ok = true
			}
			SoMsg("everything runs as expected", ok, ShouldBeTrue)
		})
	})
}
