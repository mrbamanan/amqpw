package event

import (
	"container/heap"
	"context"
	"testing"
	"time"

	// revive:disable:dot-imports
	. "github.com/smartystreets/goconvey/convey"
	// revive:enable:dot-imports
	"github.com/stretchr/testify/mock"
	"gitlab.com/qosenergy/amqpw/internal/test"
	"golang.org/x/sync/errgroup"
)

func TestTOEvent(t *testing.T) {
	Convey("test TOEvent", t, func(c C) {
		cmc := test.NewConveyMockCleanup(t, c)
		Reset(func() { cmc.Clean() })

		now := time.Now()
		mockConfirmHandler := NewMockConfirmationHandler(cmc)
		mockMsg := NewMockmsg(cmc)
		toEvt := NewTOEvent(now,
			0,
			mockConfirmHandler,
			mockMsg,
		)
		Convey("timeout", func() {
			mockConfirmHandler.On("RmDeferredConfirmation", uint64(0)).Once()
			// called on TO
			mockMsg.On("SubmitRetryBackOffEvent", mock.Anything).Once()
			toEvt.do()
			SoMsg("done", toEvt.done, ShouldBeTrue)
		})
		Convey("cancel", func() {
			mockConfirmHandler.On("RmDeferredConfirmation", uint64(0)).Once()
			// called on cancel
			mockMsg.On("Callback", false, mock.Anything).Once()
			toEvt.cancel()
			SoMsg("done", toEvt.done, ShouldBeTrue)
		})
		Convey("action, already confirmed or cancelled", func() {
			toEvt.done = true
			toEvt.do()
			SoMsg("done", toEvt.done, ShouldBeTrue)
			toEvt.cancel()
			SoMsg("done", toEvt.done, ShouldBeTrue)
		})
		Convey("when", func() {
			when := toEvt.when()
			SoMsg("when OK", when.Equal(now), ShouldBeTrue)
			SoMsg("not done", toEvt.done, ShouldBeFalse)
		})
	})
}

func TestRetryEvent(t *testing.T) {
	Convey("test RetryEvent", t, func(c C) {
		cmc := test.NewConveyMockCleanup(t, c)
		Reset(func() { cmc.Clean() })

		now := time.Now()
		mockMsg := NewMockmsg(cmc)
		rEvt := NewRetryEvent(
			now,
			mockMsg,
		)
		Convey("retry", func() {
			mockMsg.On("SubmitForPublish").Once()
			rEvt.do()
		})
		Convey("cancel", func() {
			mockMsg.On("Callback", false, mock.Anything).Once()
			rEvt.cancel()
		})
		Convey("TOEvent when", func() {
			when := rEvt.when()
			SoMsg("when OK", when.Equal(now), ShouldBeTrue)
		})
		Convey("heapId", func() {
			id := 4
			SoMsg("id init value", rEvt.getHeapID(), ShouldEqual, 0)
			rEvt.setHeapID(id)
			SoMsg("id set value", rEvt.getHeapID(), ShouldEqual, id)
		})
		Convey("EventManager", func() {
			evtManager := NewManager()
			SoMsg("evtManager init nil", rEvt.evtManagerRef, ShouldBeNil)
			rEvt.setEvtManager(evtManager)
			SoMsg("evtManager set ok", rEvt.evtManagerRef, ShouldEqual, evtManager)
		})
		Convey("Event & EventManager interactions", func() {
			evtManager := NewManager()
			// set value to check replacement in heap
			rEvt.setHeapID(-1)
			SoMsg("evtManager init nil", rEvt.evtManagerRef, ShouldBeNil)
			SoMsg("heap init size 0", len(*evtManager.eventHeap), ShouldEqual, 0)
			evtManager.AddEvent(rEvt)
			SoMsg("evtManager set", rEvt.evtManagerRef, ShouldEqual, evtManager)
			SoMsg("heap id set", rEvt.getHeapID(), ShouldEqual, 0)
			SoMsg("evt added to heap", len(*evtManager.eventHeap), ShouldEqual, 1)
			rEvt.rmFromEvtManager()
			SoMsg("evtManager removed", rEvt.evtManagerRef, ShouldBeNil)
			SoMsg("heap id -1", rEvt.getHeapID(), ShouldEqual, -1)
			SoMsg("evt removed to heap", len(*evtManager.eventHeap), ShouldEqual, 0)
		})
	})
}

func TestHeap(t *testing.T) {
	Convey("test Heap", t, func(c C) {
		cmc := test.NewConveyMockCleanup(t, c)
		Reset(func() { cmc.Clean() })

		h := make(evtHeap, 0)
		heap.Init(&h)

		Convey("Push & Pop", func() {
			now := time.Now()
			mockEvt := NewMockEvent(cmc)
			mockEvt.On("when").Return(now)
			mockEvt.On("setHeapID", mock.AnythingOfType("int")).Return()
			heap.Push(&h, mockEvt)
			SoMsg("push", len(h), ShouldEqual, 1)
			evt := heap.Pop(&h).(Event)
			SoMsg("pop", len(h), ShouldEqual, 0)
			SoMsg("value ok", evt.when().Equal(now), ShouldBeTrue)
		})

		Convey("Peek & Order", func() {
			now := time.Now()
			mockEvt := NewMockEvent(cmc)
			mockEvt.On("when").Return(now)
			mockEvt.On("setHeapID", mock.AnythingOfType("int")).Return()
			heap.Push(&h, mockEvt)
			mockEvt = NewMockEvent(cmc)
			mockEvt.On("when").Return(now.Add(-time.Second))
			mockEvt.On("setHeapID", mock.AnythingOfType("int")).Return()
			heap.Push(&h, mockEvt)
			mockEvt = NewMockEvent(cmc)
			mockEvt.On("when").Return(now.Add(2 * time.Second))
			mockEvt.On("setHeapID", mock.AnythingOfType("int")).Return()
			heap.Push(&h, mockEvt)
			mockEvt = NewMockEvent(cmc)
			mockEvt.On("when").Return(now.Add(time.Second))
			mockEvt.On("setHeapID", mock.AnythingOfType("int")).Return()
			heap.Push(&h, mockEvt)
			SoMsg("push", len(h), ShouldEqual, 4)
			SoMsg("peek", h.Peek().when(), ShouldEqual, now.Add(-time.Second))
			SoMsg("pop 1", heap.Pop(&h).(Event).when(), ShouldEqual, now.Add(-time.Second))
			SoMsg("pop 2", heap.Pop(&h).(Event).when(), ShouldEqual, now)
			SoMsg("pop 3", heap.Pop(&h).(Event).when(), ShouldEqual, now.Add(time.Second))
			SoMsg("pop 4", heap.Pop(&h).(Event).when(), ShouldEqual, now.Add(2*time.Second))
			SoMsg("done", len(h), ShouldEqual, 0)
		})
	})
}

type enrichedMockEvt struct {
	*MockEvent
	id *int
}

func (eme enrichedMockEvt) setHeapID(i int) {
	*eme.id = i
}
func (eme enrichedMockEvt) getHeapID() int {
	return *eme.id
}

func TestEventManager(t *testing.T) {
	Convey("test EventManager", t, func(c C) {
		cmc := test.NewConveyMockCleanup(t, c)
		Reset(func() { cmc.Clean() })

		evtManager := NewManager()
		Convey("with running", func() {
			cancellableCtx, cancel := context.WithCancel(context.Background())
			errGrp, errGrpCtx := errgroup.WithContext(cancellableCtx)
			testTOTimer := time.NewTimer(time.Second)
			stopWaitFunc := func() (bool, error) {
				cancel()
				waitChan := make(chan error, 1)
				go func() {
					waitErr := errGrp.Wait()
					waitChan <- waitErr
					close(waitChan)
				}()
				select {
				case <-testTOTimer.C:
					return false, nil
				case err := <-waitChan:
					return true, err
				}
			}

			Convey("single Add & Act", func() {
				now := time.Now()
				doCalled := make(chan struct{})
				mockEvt := NewMockEvent(cmc)
				mockEvt.On("when").Return(now)
				mockEvt.On("setHeapID", mock.AnythingOfType("int")).Return()
				mockEvt.On("do").Run(func(mock.Arguments) { close(doCalled) }).Once()
				mockEvt.On("setEvtManager", evtManager).Return().Once()
				mockEvt.On("setEvtManager", (*Manager)(nil)).Return().Once()
				SoMsg("nothing added yet", evtManager.eventHeap.Len(), ShouldEqual, 0)
				evtManager.AddEvent(mockEvt)
				SoMsg("add OK", evtManager.eventHeap.Len(), ShouldEqual, 1)

				evtManager.Start(errGrpCtx, errGrp)

				var ok bool
				select {
				case <-testTOTimer.C:
				case <-doCalled:
					ok = true
				}
				SoMsg("action occurred", ok, ShouldBeTrue)

				ok, err := stopWaitFunc()
				SoMsg("wait close ok", ok, ShouldBeTrue)
				SoMsg("with no error", err, ShouldBeNil)
			})

			Convey("complex operations", func() {
				now := time.Now()
				SoMsg("nothing added yet", evtManager.eventHeap.Len(), ShouldEqual, 0)
				doCalled := make(chan struct{})
				var cancelleds []int
				var dones []int
				var mockEvtToDelete enrichedMockEvt
				for i := 0; i < 10; i++ {
					n := i
					mockEvt := enrichedMockEvt{
						MockEvent: NewMockEvent(cmc),
						id:        new(int),
					}
					mockEvt.On("when").Return(now.Add(time.Duration(10-2*n) * time.Minute))
					mockEvt.On("setEvtManager", evtManager).Return().Once()
					mockEvt.On("setEvtManager", (*Manager)(nil)).Return().Once()
					switch {
					case i == 3:
						mockEvtToDelete = mockEvt
					case i >= 5:
						mockEvt.On("do").Run(
							func(mock.Arguments) {
								dones = append(dones, n)
								if n == 5 {
									close(doCalled)
								}
							},
						).Once()
					default:
						mockEvt.On("cancel").Run(func(mock.Arguments) { cancelleds = append(cancelleds, n) }).Once()
					}
					evtManager.AddEvent(mockEvt)
				}
				SoMsg("add OK", evtManager.eventHeap.Len(), ShouldEqual, 10)

				SoMsg("id set to valid value on add", *mockEvtToDelete.id, ShouldBeGreaterThanOrEqualTo, 0)
				evtManager.rmEvent(mockEvtToDelete)
				SoMsg("rm OK", evtManager.eventHeap.Len(), ShouldEqual, 9)
				SoMsg("id reset to -1 on rm", *mockEvtToDelete.id, ShouldEqual, -1)

				evtManager.Start(errGrpCtx, errGrp)

				var ok bool
				select {
				case <-testTOTimer.C:
				case <-doCalled:
					ok = true
				}
				SoMsg("action occurred", ok, ShouldBeTrue)

				ok, err := stopWaitFunc()
				SoMsg("wait close ok", ok, ShouldBeTrue)
				SoMsg("with no error", err, ShouldBeNil)

				SoMsg("dones OK", dones, ShouldEqual, []int{9, 8, 7, 6, 5})
				SoMsg("cancelleds OK", cancelleds, ShouldEqual, []int{4, 2, 1, 0})
			})
		})
	})
}
