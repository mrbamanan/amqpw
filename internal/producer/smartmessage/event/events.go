package event

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/qosenergy/amqpw/internal/common"
)

var (
	retryPool = sync.Pool{
		New: func() any {
			return &RetryEvent{}
		},
	}
	toPool = sync.Pool{
		New: func() any {
			return &TOEvent{}
		},
	}
)

/*
Event is the common interface for all kinds of event.

It can give it's trigger time, what to do when the time is elapsed,
and what to do on event manager close.
*/
type Event interface {
	when() time.Time
	do()
	cancel()

	setHeapID(int)
	getHeapID() int
	setEvtManager(*Manager)
}

// msg to call associated funcs from smartmessages
type msg interface {
	Callback(ack bool, err error)
	SubmitForPublish()
	SubmitRetryBackOffEvent(reason error)
}

// ConfirmationHandler to call associated funcs from confirmation.Handler
type ConfirmationHandler interface {
	RmDeferredConfirmation(deliveryTag uint64)
}

type eventCommon struct {
	msg      msg
	procTime time.Time

	heapID        *int
	evtManagerRef *Manager
}

func (ec *eventCommon) when() time.Time {
	return ec.procTime
}

func (ec *eventCommon) setHeapID(i int) {
	*ec.heapID = i
}

func (ec *eventCommon) getHeapID() int {
	return *ec.heapID
}

func (ec *eventCommon) setEvtManager(m *Manager) {
	ec.evtManagerRef = m
}

func (ec *eventCommon) rmFromEvtManager() {
	if ec.evtManagerRef != nil {
		ec.evtManagerRef.rmEvent(ec)
	}
}

func (*eventCommon) cancel() {
	// is reimplemented by specific events
	panic("event cancel unimplemented")
}

func (*eventCommon) do() {
	// is reimplemented by specific events
	panic("event do unimplemented")
}

// TOEvent is a publish timeout event
type TOEvent struct {
	eventCommon
	confirmationHandler ConfirmationHandler
	deliveryTag         uint64

	// internal
	mx   sync.Mutex
	done bool
}

// NewTOEvent creates a new TOEvent
func NewTOEvent(
	when time.Time,
	dTag uint64,
	confirmHandler ConfirmationHandler,
	msg msg,
) *TOEvent {
	toEvt := toPool.Get().(*TOEvent)
	toEvt.done = false
	toEvt.msg = msg
	toEvt.procTime = when
	toEvt.deliveryTag = dTag
	toEvt.confirmationHandler = confirmHandler
	toEvt.heapID = new(int)
	return toEvt
}

/*
CheckSetDone is a thread safe synchronisation func.
It sets the TO as obsolete (won't fire after this point) and
return a boolean true if the TOEvent had not fired yet.
*/
func (to *TOEvent) CheckSetDone() bool {
	to.mx.Lock()
	oldDone := to.done
	if !oldDone {
		// first call: TO won't happen anymore
		// remove it from event manager if not removed yet
		to.rmFromEvtManager()
	}
	to.done = true
	to.mx.Unlock()
	return oldDone
}

func (to *TOEvent) do() {
	// we ensure all references where removed in the func
	defer toPool.Put(to)

	if to.CheckSetDone() {
		// message already received confirmation: do nothing
		return
	}

	// remove from confirmation handler
	to.confirmationHandler.RmDeferredConfirmation(to.deliveryTag)
	to.msg.SubmitRetryBackOffEvent(common.SetRetriableErr(fmt.Errorf("timeout")))
}

func (to *TOEvent) cancel() {
	// we ensure all references where removed in the func
	defer toPool.Put(to)

	if to.CheckSetDone() {
		// message already received confirmation: do nothing
		return
	}

	// remove from confirmation handler
	to.confirmationHandler.RmDeferredConfirmation(to.deliveryTag)
	to.msg.Callback(false, nil)
}

// RetryEvent is a publish retry event
type RetryEvent struct {
	eventCommon
}

// NewRetryEvent creates a new RetryEvent
func NewRetryEvent(when time.Time, msg msg) *RetryEvent {
	rEvt := retryPool.Get().(*RetryEvent)
	rEvt.msg = msg
	rEvt.procTime = when
	rEvt.heapID = new(int)
	return rEvt
}

func (re *RetryEvent) do() {
	re.msg.SubmitForPublish()
	retryPool.Put(re)
}

func (re *RetryEvent) cancel() {
	re.msg.Callback(false, nil)
	retryPool.Put(re)
}
