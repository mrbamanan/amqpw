package event

// evtHeap ensures the next event to trigger is on top
type evtHeap []Event

func (h evtHeap) Len() int           { return len(h) }
func (h evtHeap) Less(i, j int) bool { return h[i].when().Before(h[j].when()) }
func (h evtHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
	h[i].setHeapID(i)
	h[j].setHeapID(j)
}

func (h *evtHeap) Push(x any) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	e := x.(Event)
	e.setHeapID(h.Len())
	*h = append(*h, e)
}

func (h *evtHeap) Pop() any {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	x.setHeapID(-1)
	return x
}

func (h *evtHeap) Peek() Event {
	return (*h)[0]
}
