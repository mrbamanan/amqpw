// Package consumer regarding rabbit messages consumption
package consumer

import (
	"context"
	"fmt"
	"io"
	"sync"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/rabbitmq/amqp091-go"

	"gitlab.com/qosenergy/amqpw/internal/common"
)

// Conf regroups the informations required by the consumer to know how to run
type Conf struct {
	ChannelPoolSize int
	QOS             *common.QOS
}

// ConsumeOptions abstracts the optional options that can be passed to the Consume options.
// If not given, all values are considered false/empty
type ConsumeOptions struct {
	AutoAck   bool
	Exclusive bool
	NoWait    bool
	Args      amqp091.Table
}

/*
RabbitMQConsumer implements Consumer interface and represents an amqp channel.
Caller can use this channel to consume on a unique queue
which can be created at consumer initialization before starting consuming deliveries.
For convenience, it is possible to define a default exchange at initialization as well.
A RabbitMQConsumer is identified by its 'tag'. Make sure to use unique tag when you create a new RabbitMQConsumer.
*/
type RabbitMQConsumer struct {
	ctx context.Context
	QOS *common.QOS
	// list of reconnector to cycle through on new consumer (reconnection will keep the same)
	reChannels []common.ChannelReconnector
	nextChanID int

	consumersMX     sync.Mutex
	backoffBuilder  func() backoff.BackOff
	activeConsumers map[string]common.Channel
}

// NewRabbitMQConsumer returns a RabbitMQConsumer struct
func NewRabbitMQConsumer(
	ctx context.Context,
	conn common.ConnReconnector,
	config Conf,
) *RabbitMQConsumer {
	poolSize := 1
	if config.ChannelPoolSize > 1 {
		poolSize = config.ChannelPoolSize
	}
	reChans := make([]common.ChannelReconnector, poolSize)
	for i := 0; i < poolSize; i++ {
		reChans[i] = conn.NewReChannel()
	}

	return &RabbitMQConsumer{
		ctx:        ctx,
		reChannels: reChans,
		QOS:        config.QOS,

		// backoff builder replacable in tests
		backoffBuilder: func() backoff.BackOff {
			expBackoff := backoff.NewExponentialBackOff()
			expBackoff.MaxInterval = 10 * time.Second
			expBackoff.MaxElapsedTime = 1 * time.Minute
			return expBackoff
		},
		activeConsumers: make(map[string]common.Channel),
	}
}

/*
Consume returns consumer deliveries channel on specified queue.

If consumer context is closed, the consumption will stop and the output chan will close.
If a disconnection occures, reconnection is automatic and invisible.
If an error occures when reconnecting, the error will be sent to the errorChan (connection/broker),
and the out channel will be closed.
Further retry policies should be handled by the caller of Consume().

The tag must be unique.
Using the same tag than a running consumer will lead to unpredictable behaviours.
Using a different tag with the same bindings will register one more consumer on the same queue.
*/
func (c *RabbitMQConsumer) Consume(
	queue string, tag string, options *ConsumeOptions,
) (<-chan amqp091.Delivery, error) {
	select {
	case <-c.ctx.Done():
		return nil, io.EOF
	default:
	}

	if options == nil {
		options = &ConsumeOptions{Args: amqp091.Table{}}
	}

	reChan := c.reChannels[c.nextChanID]
	c.nextChanID++
	if c.nextChanID >= len(c.reChannels) {
		c.nextChanID = 0
	}

	// first consume is synchronous to immediately detect errors from config
	c.consumersMX.Lock()
	inChan, consumeErr := c.consume(reChan, queue, tag, *options)
	c.consumersMX.Unlock()
	if consumeErr != nil {
		return nil, fmt.Errorf("initial consume: %w", consumeErr)
	}

	outChan := make(chan amqp091.Delivery, 1)

	go func() {
		for {
			closing := c.forwardActiveConsume(reChan, inChan, outChan, tag)

			if !closing {
				inChan, closing = c.retryConsume(reChan, queue, tag, *options)
			}

			if closing {
				close(outChan)
				return
			}
		}
	}()

	return outChan, nil
}

/*
Cancel tries canceling the consumption for the given tag.
In case of success, the chan of the given consumer will be closed once all remaining reserved
messages are consumed.

If a Retriable error is returned, a network error occurred and the consumer was not cancelled.
If it is not Retriable, then there was no active consumer with this tag.
*/
func (c *RabbitMQConsumer) Cancel(tag string) error {
	c.consumersMX.Lock()
	defer c.consumersMX.Unlock()
	if amqpChannel, ok := c.activeConsumers[tag]; ok {
		// if consume is currently active, cancel it (this closes inChan)
		if amqpChannel != nil {
			err := common.NotNilWrap("manual cancel: %w", amqpChannel.Cancel(tag, false), common.SetRetriableErr)
			if err != nil {
				return err
			}
		}
		// setup consumer to close after canceling the channel if it was active
		delete(c.activeConsumers, tag)
		return nil
	}
	return fmt.Errorf("No active consumer found for tag %s", tag)
}

func (c *RabbitMQConsumer) consume(
	reChan common.ChannelReconnector,
	queue string, tag string, options ConsumeOptions,
) (<-chan amqp091.Delivery, error) {
	amqpChannel, chErr := reChan.Channel(c.QOS, false)
	if chErr != nil {
		return nil, fmt.Errorf("getting amqp channel: %w", chErr)
	}

	// start consuming deliveries on queue
	inChan, consumeErr := amqpChannel.Consume(
		queue,
		tag,
		options.AutoAck,
		options.Exclusive,
		false,
		options.NoWait,
		options.Args,
	)

	c.activeConsumers[tag] = amqpChannel

	return inChan, common.NotNilWrap("consuming from amqp channel: %w", consumeErr)
}

func (c *RabbitMQConsumer) forwardActiveConsume(
	reChan common.ChannelReconnector,
	inChan <-chan amqp091.Delivery,
	outChan chan<- amqp091.Delivery,
	tag string,
) bool {
	mustCloseConsumer := true
	mustNotCloseConsumer := false
	// ***** exit if no active consumer *****
	for {
		select {
		// ***** consume inChan *****
		case delivery, open := <-inChan:
			if !open {
				// mark consumer as disconnected if not open
				c.consumersMX.Lock()
				if _, ok := c.activeConsumers[tag]; ok {
					c.activeConsumers[tag] = nil
				} else {
					c.consumersMX.Unlock()
					return mustCloseConsumer
				}
				c.consumersMX.Unlock()
				return mustNotCloseConsumer
			}
			outChan <- delivery
			// ***** handle context Done while consuming *****
		case <-c.ctx.Done():
			// on context done: Cancel amqp channel & finish consuming
			c.consumersMX.Lock()
			activeChannel, ok := c.activeConsumers[tag]
			c.consumersMX.Unlock()
			if !ok {
				reChan.SendErr(
					fmt.Errorf("no active consume \"%s\" on ctx Done (should not happen)", tag),
				)
				return mustCloseConsumer
			}
			// try proper cancel
			cancelErr := activeChannel.Cancel(tag, false)
			if cancelErr != nil {
				reChan.SendErr(
					fmt.Errorf("cancel consume \"%s\" on ctx Done: %w", tag, cancelErr),
				)
				return mustCloseConsumer
			}
			// if cancel ok, finish consuming last messages
			for delivery := range inChan {
				outChan <- delivery
			}
			return mustCloseConsumer
		}
	}
}

func (c *RabbitMQConsumer) retryConsume(
	reChan common.ChannelReconnector,
	queue, tag string,
	options ConsumeOptions,
) (inChan <-chan amqp091.Delivery, closing bool) {
	var consumeCount int
	retryPolicy := c.backoffBuilder()
	for {
		// ***** stop retry if consumer was cancelled or ctx done *****
		c.consumersMX.Lock()
		if _, ok := c.activeConsumers[tag]; !ok {
			// Cancel was called and inChan emptied: close consumer
			c.consumersMX.Unlock()
			return nil, true
		}
		select {
		case <-c.ctx.Done():
			delete(c.activeConsumers, tag)
			c.consumersMX.Unlock()
			return nil, true
		default:
		}
		c.consumersMX.Unlock()

		// ***** try consuming *****

		// A limit to the retries was set to avoid endless silent softlock
		c.consumersMX.Lock()
		inChan, consumeErr := c.consume(reChan, queue, tag, options)
		c.consumersMX.Unlock()
		if consumeErr == nil {
			// consume OK: reset the retry policy
			return inChan, false
		}

		// ***** if consume failed, handle retry backoff *****
		consumeCount++
		nextBO := retryPolicy.NextBackOff()
		if nextBO == backoff.Stop {
			consumeErr = fmt.Errorf("stop after last retry: %w", consumeErr)
		}
		reChan.SendErr(
			fmt.Errorf(
				"retry consume after disconnect (retry %d): %w",
				consumeCount,
				consumeErr,
			),
		)
		if nextBO == backoff.Stop {
			c.consumersMX.Lock()
			delete(c.activeConsumers, tag)
			c.consumersMX.Unlock()
			return nil, true
		}
		// sleep until next try (short so uninterruptable by context.Done)
		time.Sleep(nextBO)
	}
}
