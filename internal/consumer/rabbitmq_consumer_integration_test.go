//go:build integration

package consumer

import (
	"context"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/qosenergy/amqpw/internal/connection"
	"gitlab.com/qosenergy/amqpw/internal/test"
)

func TestRabbitMQConsumer_Consume(t *testing.T) {
	assertion := require.New(t)

	uri, ok := os.LookupEnv(test.AmqpURIKey)
	assertion.True(ok, fmt.Sprintf("%q env var missing", test.AmqpURIKey))

	queue := "q.test"
	body := []byte("test")

	err := test.Publish(uri, queue, body)
	assertion.NoError(err, "publishing test delivery")

	rco := connection.NewReconnector(uri, "test consumer conn", nil)

	consr := NewRabbitMQConsumer(context.Background(), rco, Conf{})

	deliveryCh, err := consr.Consume(queue, "", nil)
	assertion.NoError(err, "consume")

	delivery := <-deliveryCh

	assertion.Equal(body, delivery.Body, "consuming message")
	assertion.NoError(delivery.Ack(false), "ack ok")
	assertion.NoError(rco.Close(), "close conn")
}
