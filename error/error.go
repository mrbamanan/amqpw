// Package amqpwerror exposes function to ease error handling.
// It should automatically alias as amqpwerror to avoid conflict with native go lib.
package amqpwerror

import (
	"gitlab.com/qosenergy/amqpw/internal/common"
)

// IsErrRetriable returns true if the error is retriable (or wraps a retriable)
func IsErrRetriable(err error) bool {
	return common.IsErrRetriable(err)
}

/*
IsErrPublishRejected returns true if the error is from a reject-publish (it is also always retriable)

This func can only be used on a publish statusChan error (to add context),
or to check the returned error of a "not ASync" Publish.
Reject-publish errors in ASync callbacks are replaced by nil:
callback with (false ack & nil error) mean it's a reject publish.
*/
func IsErrPublishRejected(err error) bool {
	return common.IsErrPublishRejected(err)
}
